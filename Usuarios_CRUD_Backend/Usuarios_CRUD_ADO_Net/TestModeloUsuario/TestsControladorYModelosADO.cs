using ModeloUsuarios;
using NUnit.Framework;

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using Usuarios_CRUD_WCF.Controllers;

namespace TestModeloUsuario
{
    public class TestsControladorYModelosADO
    {
        ModeloUsuario modelo;
        ModuloPersistenciaADO moduloPersistencia;
        private string CONEX_BD = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\DB\\bd_usu_test.mdf;Integrated Security=True;Connect Timeout=30";

        public static int id1, id2, id3;

        void ResetearModeloYCtrl_ConReflection()
        {
            // FieldInfo[] camposModelo = ModeloUsuario.Instancia.GetType().GetFields(...);
            FieldInfo[] camposModelo = typeof(ModeloUsuario).GetFields(
                BindingFlags.NonPublic | BindingFlags.Static);
            foreach (FieldInfo campo in camposModelo)
            {
                if (campo.Name.Equals("instance"))
                {
                    ModeloUsuario.Instancia.ToString();
                    Console.WriteLine("Encontrado instance Modelo y eliminado");
                    campo.SetValue(null, null);
                    ModeloUsuario.Instancia.ToString();
                }
            }
            moduloPersistencia = new ModuloPersistenciaADO(Entorno.Desarrollo);
            modelo = ModeloUsuario.Instancia;
        }


        [SetUp]
        public void Setup()
        {
            moduloPersistencia = new ModuloPersistenciaADO(Entorno.Desarrollo);
            modelo = ModeloUsuario.Instancia;
            //ultimoIdInicial = GetMayorId();
        }
        [TearDown]
        public void AlTerminarTest()
        {
            EliminarUsuariosValidos();
        }

        void CrearUsuariosValidos()
        {
            id1 = modelo.Crear(new Usuario("Primero", 27, 1.5f));
            id2 = modelo.Crear(new Usuario("Segundo", 33, 2));
            id3 = modelo.Crear(new Usuario("Tercero", 45, 2));
        }
        void EliminarUsuariosValidos()
        {
            modelo.Eliminar(id1);
            modelo.Eliminar(id2);
            modelo.Eliminar(id3);
        }



        [Test]
        public void TestCrearValidos()
        {
            int numTotalUsers = modelo.LeerTodos().Count;
            int idAdios = modelo.Crear(new Usuario("adios2", 3, 2.2f));
            int idQtal = modelo.Crear(new Usuario("qtal2", 400, 27f));
            Usuario userAdios2 = modelo.LeerUno(idAdios);
            Usuario userQtal2 = modelo.LeerUno(idQtal);
            AssertUser(userAdios2, "adios2", 3, 2.2f,"Diferencias entre el usuario introducido y el usuario con su id en la tabla");
            AssertUser(userQtal2, "qtal2", 1, 1f, "Diferencias entre el usuario introducido y el usuario con su id en la tabla(puede ser que la edad o altura del usuario fueran ilegales)");
            Assert.Null(modelo.LeerUno(idQtal+1),"usuario con un id mayor al �ltimo introducido");
            Assert.AreEqual(modelo.LeerTodos().Count, numTotalUsers+2, "Numero de usuarios creados durante el test incorrecto");
            modelo.Eliminar(idAdios);
            modelo.Eliminar(idQtal);
        }

        private void AssertUser(Usuario user, string nombre, int edad, float altura, string msg)
        {
            Assert.AreEqual(nombre, user.Nombre,msg);
            Assert.AreEqual(edad, user.Edad,msg);
            Assert.AreEqual(altura, user.Altura,msg);
        }

        [Test]
        public void TestModificar()
        {
            CrearUsuariosValidos();

            Usuario user1 = modelo.LeerUno(id1);
            user1.Nombre = "Nuevo Nombre";
            user1.Edad = 45;
            user1.Altura = 2.5f;
            Assert.IsTrue(modelo.Modificar(user1),"Error al modificar el usuario");
            Usuario nuevoUser = modelo.LeerUno(id1);
            AssertUser(user1, nuevoUser.Nombre, nuevoUser.Edad, nuevoUser.Altura, "No se han modificado correctamente los valores del usuario con id: " + id1 + " en la BD");
        }


        [Test]
        public void TestEliminar()
        {
            CrearUsuariosValidos();
            int size = modelo.LeerTodos().Count;
            Assert.IsTrue(modelo.Eliminar(id1), "No se ha eliminado bien un usuario");
            Assert.IsTrue(modelo.Eliminar(id2), "No se ha eliminado bien un usuario");
            Assert.IsTrue(modelo.Eliminar(id3), "No se ha eliminado bien un usuario");
            Assert.IsTrue(!modelo.Eliminar(id1), "No ha respondido bien ante un intento de eliminar un usuario inexistente");
            Assert.IsTrue(!modelo.Eliminar(id2), "No ha respondido bien ante un intento de eliminar un usuario inexistente");
            Assert.IsTrue(!modelo.Eliminar(id3), "No ha respondido bien ante un intento de eliminar un usuario inexistente");
            Assert.IsTrue(!modelo.Eliminar(id3+1), "No ha respondido bien ante un intento de eliminar un usuario inexistente");
            Assert.AreEqual(modelo.LeerTodos().Count, size - 3, "No se han eliminado bien los 3 usuarios");
        }

        [Test]
        public void TestLeerUno()
        {
            CrearUsuariosValidos();
            AssertUser(modelo.LeerUno(id1), "Primero", 27, 1.5f, "Primer usuario leido de forma incorrecta");
            AssertUser(modelo.LeerUno(id2), "Segundo", 33, 2, "Segundo usuario leido de forma incorrecta");
            AssertUser(modelo.LeerUno(id3), "Tercero", 45, 2, "Tercer usuario leido de forma incorrecta");
            Assert.Null(modelo.LeerUno(id3+1));
            Assert.NotNull(modelo.LeerUno(id1));
            Assert.IsInstanceOf<Usuario>(modelo.LeerUno(id1));
            EliminarUsuariosValidos();

        }

        [Test]
        public void TestLeerTodos()
        {
            CrearUsuariosValidos();
            IList<Usuario> lista = modelo.LeerTodos();
            int bd_original = getNumUsers();
            Assert.AreEqual(bd_original, lista.Count);
            AssertUser(lista[lista.Count-1], "Tercero", 45, 2, "Tercer usuario leido de forma incorrecta");
            modelo.Eliminar(id1);
            Assert.AreEqual(bd_original - 1, modelo.LeerTodos().Count);
        }

        public int getNumUsers()
        {
            int numUsers = 0;
            try
            { 
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "SELECT COUNT(*) as numUsers FROM USUARIO";

                    SqlDataReader lectorDR = comando.ExecuteReader();

                    while (lectorDR.Read())
                    {

                        numUsers = (int)lectorDR["numUsers"];

                    }
                    return numUsers;

                }   // conexion.Close();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
            }
            return numUsers; 
        }
    }

}
