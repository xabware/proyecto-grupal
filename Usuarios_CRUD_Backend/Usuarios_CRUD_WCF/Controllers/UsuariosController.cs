﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ModeloUsuarios;
using System.Collections.Generic;
using System.Text;

namespace Usuarios_CRUD_WCF.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsuariosController : ControllerBase
    {
        IModeloGenerico<Usuario> modeloUsuario;

        private readonly ILogger<UsuariosController> _logger;

        public UsuariosController(ILogger<UsuariosController> logger)
        {
            this.modeloUsuario = ModeloUsuario.Instancia;
            this._logger = logger;
        }

        

        [HttpPost("crear/{nuevoObj}")]
        public int Crear(Usuario nuevoObj)
        {
            return modeloUsuario.Crear(nuevoObj);
        }

        [HttpGet]
        public IList<Usuario> LeerTodos()
        {
            return modeloUsuario.LeerTodos();
        }

        [HttpDelete("eliminar/{entero}")]
        public bool Eliminar(int entero)
        {
            return modeloUsuario.Eliminar(entero);
        }

        [HttpPost("leer/{entero}")]
        public Usuario LeerUno(int entero)
        {
            return modeloUsuario.LeerUno(entero);
        }
        /*public bool LeerConfirmar(string nombre)
        {
            return modeloUsuario.LeerConfirmar(nombre);
        }*/

        [HttpPost("modificar/{usuario}")]
        public void Modificar(Usuario usuario)
        {
            modeloUsuario.Modificar(usuario);
        }
    }
}
