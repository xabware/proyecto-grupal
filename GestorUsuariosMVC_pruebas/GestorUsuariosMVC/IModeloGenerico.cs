﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestorUsuariosMVC
{
    public interface IModeloGenerico<Tipo>
    {
        Tipo Crear(Tipo nuevoObj);
        IList<Tipo> LeerTodos();
        Tipo LeerUno<TipoBusq>(TipoBusq campoBusq);
        bool Eliminar<TipoBusq>(TipoBusq campoBusq);
        Tipo Modificar<TipoBusq>(TipoBusq campoBusq,Tipo nuevoObjeto);

    }
}
