﻿using System;

namespace GestorUsuariosMVC
{
    class Program
    {
        IModeloGenerico<Usuario> modelo;
        Controlador controlador;
        IVistaGenerica vista;
        Persistencia pers;
        VistaEventos vistaE;

        public Program()
        {
            modelo = Modelo.Instancia;
            pers = new Persistencia();
            vistaE = new VistaEventos();
            controlador = new Controlador(modelo);
            vista = new Vista(controlador);
        }

        static void Main(string[] args)
        {
            Program programa = new Program();
            programa.vista.Menu();
        }
    }
}
