﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestorUsuariosMVC
{
    public interface INombrable
    {

        string GetNombre();

        void SetNombre(string nombre);

        string Nombre
        {
            get;
            set;
        }

    }
}
