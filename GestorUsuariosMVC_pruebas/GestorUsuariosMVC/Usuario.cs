﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestorUsuariosMVC
{
    public class Usuario : Object, INombrable
    {

        string nombre;
        float altura;
        int edad;

        public Usuario(string nombre, float altura, int edad)
        {
            this.nombre = nombre;
            this.altura = altura;
            this.edad = edad;
        }

        public Usuario()
        {
            this.nombre = "nada";
            this.altura = 1.5f;
            this.edad = 20;
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value == "" ? "vacio" : (value ?? "nulo"); }
        }

        public float Altura
        {
            get { return altura; }
            set { altura = value < 0.1f ? 0.1f : value; }
        }

        public int Edad
        {
            get { return edad; }
            set { edad = value < 1 ? 1 : value; }
        }

        public override string ToString()
        {
            return Nombre + " - " + Altura.ToString() + " - " + Edad.ToString();
        }

        public string GetNombre()
        {
            throw new NotImplementedException();
        }

        public void SetNombre(string nombre)
        {
            throw new NotImplementedException();
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj))
            {
                return true;
            }
            else
            {
                Usuario u = (Usuario)obj;
                return (this.Altura == u.Altura && this.Edad == u.Edad && this.Nombre == u.Nombre);
            }
        }
    }
}
