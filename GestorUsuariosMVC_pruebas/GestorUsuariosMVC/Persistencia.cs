﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace GestorUsuariosMVC
{
    public class Persistencia
    {
        public static string path = Directory.GetCurrentDirectory()+"\\..\\..\\..\\losUsuarios.txt";
        public Persistencia()
        {
            Modelo modelo = Modelo.Instancia;
            modelo.OnCreate += (Usuario user) =>
            {
                string yeison = JsonSerializer.Serialize<Usuario>(user)+"\n";
                File.AppendAllText(path, yeison);
            };

            modelo.loadUsers += () =>
            {
                List<Usuario> lista = new List<Usuario>();
                lista = cargarDeFichero();
                return lista;
            };

            modelo.OnDelete += (string str) =>
            {
                List<Usuario> lista = new List<Usuario>();
                lista = cargarDeFichero();
                string nuevoFichero = "";
                Usuario usuario = lista.Find(u => u.Nombre == str);
                lista.Remove(usuario);
                nuevoFichero = serializar(lista);
                File.Delete(path);
                File.AppendAllText(path, nuevoFichero);
            };

            modelo.OnChange += (string str,Usuario nuevo) =>
            {
                List<Usuario> lista = new List<Usuario>();
                lista = cargarDeFichero();
                string nuevoFichero = "";
                Usuario usuario = lista.Find(u => u.Nombre == str);
                if(usuario == null)
                {
                    return;
                }
                lista[lista.IndexOf(usuario)] = nuevo;
                nuevoFichero = serializar(lista);
                File.Delete(path);
                File.AppendAllText(path, nuevoFichero);
            };
        }

        

        private string serializar(List<Usuario> lista)
        {
            string res="";
            foreach (Usuario user in lista)
            {
                res += (JsonSerializer.Serialize<Usuario>(user) + "\n");
            }
            return res;
        }

        private List<Usuario> cargarDeFichero()
        {
            List<Usuario> lista= new List<Usuario>();
            if (File.Exists(path))
            {
                Usuario user;
                System.IO.StreamReader file = null;
                string line = "";
                file = new System.IO.StreamReader(path);
                while ((line = file.ReadLine()) != null)
                {
                    user = JsonSerializer.Deserialize<Usuario>(line);
                    lista.Add(user);
                }
                file.Close();
            }
            return lista;
            
        }
    }
}
