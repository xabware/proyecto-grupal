﻿
using System;
using System.Collections.Generic;

using System.Text;

namespace GestorUsuariosMVC
{
    class VistaEventos
    {
        

        public VistaEventos()
        {
            Modelo modelo = Modelo.Instancia;
            modelo.OnCreate += (Usuario user) =>
            {
                Comprobador.mensajeColor("Se ha creado el usuario " + user.Nombre, ConsoleColor.Cyan);
            };
            modelo.OnDelete += (string str) =>
            {
                Comprobador.mensajeColor("Se ha eliminado el usuario " + str, ConsoleColor.Cyan);
            };
            modelo.OnChange += (string str,Usuario user) =>
            {
                Comprobador.mensajeColor("Se ha cambiado el usuario " + str, ConsoleColor.Cyan);
            };
            modelo.OnRead += (string str) =>
            {
                Comprobador.mensajeColor("Se ha leído el usuario " + str, ConsoleColor.Cyan);
            };
            modelo.onReadAll += () =>
            {
                Comprobador.mensajeColor("Se han leido todos los usuarios", ConsoleColor.Cyan);
            };
        }
    }
}
