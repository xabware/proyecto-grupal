﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestorUsuariosMVC
{
    public interface IVistaGenerica
    {
        public void Menu();
        public void VCrear();
        public void VMostrarUno();
        public void VMostrarTodos();
        public void VModificar();
        public void VEliminar();

    }
}
