using NUnit.Framework;
using GestorUsuariosMVC;
using System.Collections;
using System.Reflection;
using System;

namespace GestorUsuariosMVC_Test
{
    public class Tests
    {
        IModeloGenerico<Usuario> modelo;
        Controlador controlador;
        Persistencia pers;

        [SetUp]
        public void Setup()
        {
            modelo = Modelo.Instancia;
            pers = new Persistencia();
            controlador = new Controlador(modelo);
            controlador.CCrear("Paula", 1.60f, 22);
            controlador.CCrear("Urbez", 1f, 50);
            controlador.CCrear("Xabier", 1.83f, 21);
        }


        [Test]
        public void TestCrearUsuarios()
        {
            int original = getNLista();
            Assert.True(controlador.CCrear("Juan", 2.4f, 10000).Equals(new Usuario("Juan", 2.4f, 10000)));
            Assert.False(controlador.CCrear("HorseLuis", 2.5f, 10001).Equals(new Usuario("Juan", 2.4f, 10000)));
            Assert.True(getNLista() == original+2);
            ResetearModeloyCtrl_ConReflection();
        }

        [Test]
        public void TestLeerUno()
        {
            Assert.True(controlador.CLeerUno("Paula").Equals(new Usuario("Paula", 1.60f, 22)));
            Assert.IsNull(controlador.CLeerUno("Esternocleidomastoideo"));
        }

        [Test]
        public void TestEliminarUsuarios()
        {
            Assert.False(controlador.CEliminar("pedrito"));
            Assert.False(controlador.CEliminar("Pepe"));
            Assert.True(controlador.CEliminar("Urbez"));
            Assert.True(controlador.CEliminar("Xabier"));
        }

        

        [Test]
        public void TestEditarUsuarios()
        {
            int i = getNLista();
            Assert.IsNull(controlador.CModificar("No Existo", "", 1, 1));
            Assert.True(controlador.CModificar("Paula", "Juan meets Man", 2f, 100).Equals(new Usuario("Juan meets Man", 2f, 100)));
            Assert.True(controlador.CModificar("Urbez", "Urbez", 4f, 50).Equals (new Usuario("Urbez", 4f, 50)));
            Assert.True(controlador.CModificar("Xabier", "Xabier", 1.83f, 100).Equals (new Usuario("Xabier", 1.83f, 100)));
            Assert.True(controlador.CModificar("Urbez", "Xabier", 1.83f, 100).Equals(new Usuario("Xabier", 1.83f, 100)));
            Assert.True(controlador.CModificar("Juan meets Man", "Juan meets Man", 10f, 31).Equals(new Usuario("Juan meets Man", 10f, 31)));
            Assert.True(i == getNLista());
        }

        private int getNLista()
        {
            IEnumerable lista = controlador.CLeerTodos();
            int i = 0;
            foreach (Usuario usuario in lista)
            {
                i++;
            }
            return i;
        }

        void ResetearModeloyCtrl_ConReflection()
        {
            FieldInfo[] camposModelo = typeof(Modelo).GetFields(BindingFlags.NonPublic | BindingFlags.Static);
            foreach (FieldInfo campo in camposModelo)
            {
                if (campo.Name.Equals("instancia"))
                {
                    Console.WriteLine("Encontrado instance");
                    campo.SetValue(null, null);
                    modelo = Modelo.Instancia;
                }
            }
            controlador = new Controlador(modelo);
        }

        void EliminarTodosUsuarios()
        {
            while (Modelo.Instancia.LeerTodos().Count > 0)
            {
                Modelo.Instancia.Eliminar(Modelo.Instancia.LeerTodos()[0].Nombre);
            }

        }

        [TearDown]
        public void AlTerminarTest()
        {
            //ResetearModeloyCtrl_ConReflection();
            EliminarTodosUsuarios();
            LimpiarJSON();
        }

        void LimpiarJSON()
        {
            Console.WriteLine(Persistencia.path);
            System.IO.File.Delete(Persistencia.path);
        }

    }
}