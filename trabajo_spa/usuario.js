class Usuario {
    constructor(nombre, edad, altura, activo) {
        this.nombre = nombre;
        this.edad = edad;
        this.altura = altura;
        this.activo = activo;
    }

    getNombre(){
        return this.nombre;
    }

    getEdad(){
        return this.edad;
    }

    getAltura(){
        return this.altura;
    }

    getActivo(){
        return this.activo;
    }

    setNombre(nombre){
        this.nombre = nombre;
    }

    setEdad(edad){
        this.edad = edad;
    }

    setAltura(altura){
        this.altura = altura;
    }

    setActivo(activo){
        this.activo = activo;
    }
}