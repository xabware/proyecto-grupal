class Modelo {

    constructor() {
        this.actualizar();
        this.alActualizar = () => {}
    }

    actualizar() {
        this.usuarios = [];
        this.cargarUsuarios();
    }

    borrarUsuario(nombre) {
        var confirmacion = confirm("¿Seguro que quieres eliminar el usuario " + nombre + "?");
        if (!confirmacion) {
            return;
        }

        for (let i in this.usuarios) {
            if (this.usuarios[i]["nombre"] === nombre) {
                this.usuarios.splice(i, 1);
                break;
            }
        }

        this.guardarUsuarios();
    }

    borrarTodo() {
        var confirmacion = confirm("¿Seguro que quieres eliminar todo?");
        if (!confirmacion) {
            return;
        }

        this.usuarios.splice(0, this.usuarios.length);
        this.guardarUsuarios();
    }

    cancelar() {
        this.editar = [];
        this.guardarUsuarios();
    }

    modificarUsuario(nuevoNombre, edad, altura, activo) {
        for (let i in this.usuarios) {
            if (this.usuarios[i]["nombre"] === this.editar["nombre"]) {
                this.usuarios[i]["nombre"] = nuevoNombre;
                this.usuarios[i]["edad"] = edad;
                this.usuarios[i]["altura"] = altura;
                this.usuarios[i]["activo"] = activo;
                this.editar = [];
                this.guardarUsuarios();
                return;
            }
        }
        this.actualizar();
        alert("No encontrado: " + this.editar["nombre"]);
    }

    cargarModificar(nombre) {
        for (let i in this.usuarios) {
            if (this.usuarios[i]["nombre"] === nombre) {
                this.editar = this.usuarios[i];
                this.guardarUsuarios();
                return;
            }
        }
        alert("No encontrado");
    }

    buscarNombre(nombre) {
        for (let i in this.usuarios) {
            if (this.usuarios[i]["nombre"].toUpperCase() === nombre.toUpperCase()) {
                return true;
            }
        }
        return false;
    }

    getUsuarios() {
        return this.usuarios;
    }

    crearUsuario(nombre, edad, altura, activo) {
        this.usuarios.push(new Usuario(nombre, edad, altura, activo));

        let opcionesPOST = {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json"
                // "Content-Type": "application/x-www-form-urlencoded"
            },
            body: ` {
                "nombre": "${nombre}",
                "edad": ${edad},
                "altura":${altura},
                "activo": ${activo}
              }`
        };

        let promesaAJAX = fetch("http://localhost:21902/api/Usuarios/crear/swdf", opcionesPOST);

        promesaAJAX
            .then((respuesta) => respuesta.json())
            .then(objWF => { // Callback de la llamada asincrona
                if (objWF != -1) {
                    console.log("usuario insertado");
                    this.alActualizar(this.usuarios, []);
                } else {
                    console.error("ERROR al insertar usuario");
                }
            })
            .catch(error => {
                console.error("ERROR en petición: " + error);
            });




    }


    guardarUsuarios() {
        let jsonUsuarios = {
            "usuarios": this.usuarios,
            "editar": this.editar
        }
        window.localStorage.setItem("datos", JSON.stringify(jsonUsuarios));
    }

    cargarUsuarios() {

        let opcionesPOST = {
            method: "GET",
            mode: "cors",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json"
                // "Content-Type": "application/x-www-form-urlencoded"
            },
        };
        // Esta otra forma de usar AJAX, devuelve una promesa.
        let promesaAJAX = fetch("http://localhost:21902/api/Usuarios", opcionesPOST);
        // En el futuro, unos segundos después, traerá datos.
        // Para entonces, queremos parsear el JSON mediante una función transformadora
        // y usar el objeto JS como en el anterior ejemplo

        promesaAJAX
            .then((respuesta) => respuesta.json())
            .then(tmpUsuarios => { // Callback de la llamada asincrona
                this.usuarios = tmpUsuarios;
                this.alActualizar(this.usuarios, []);
            })
            .catch(error => {
                console.error("ERROR en petición: " + error);
            });

        console.log(this.usuarios);
    }
}