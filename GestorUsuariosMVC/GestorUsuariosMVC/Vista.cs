﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestorUsuariosMVC
{
    class Vista : IVistaGenerica
    {
        Controlador controlador;

        public Vista(Controlador controlador)
        {
            this.controlador = controlador;
        }

        public void Menu()
        {
            do
            {
                int opcion = PedirOpcion("Error, tu opción debe ser 1, 2, 3, 4 o 5.");
                GestionarOpcion(opcion);
            } while (true);
        }

        public void VCrear()
        {
            Console.Write("Introduce el nombre del usuario: ");
            string nombre = Console.ReadLine();
            float altura = Comprobador.IntroducirNumero<float>("Introduce la altura del usuario: ", "Error, el dato debe ser un número real");
            int edad = Comprobador.IntroducirNumero<int>("Introduce la edad del usuario: ", "Error, el dato debe ser un número entero");
            Usuario us = this.controlador.CCrear(nombre, altura, edad);
            if (us != null)
            {
                Comprobador.mensajeColor("Se dio de alta el usuario " + us.ToString(), ConsoleColor.Green);
            }
            else
            {
                Comprobador.mensajeColor("No se dio de alta el usuario " + nombre, ConsoleColor.Red);
            }
        }

        public void VEliminar()
        {
            Usuario usuario;
            Console.Write("Intrduce el nombre del usuario que deseas eliminar: ");
            string nombre = Console.ReadLine();
            usuario = controlador.CLeerUno(nombre);
            if (usuario != null)
                if(controlador.CEliminar(nombre))
                    Comprobador.mensajeColor("Eliminado correctamente", ConsoleColor.Green);
            else
                Comprobador.mensajeColor("No encontrado por " + nombre, ConsoleColor.Red);
        }

        public void VModificar()
        {
            Usuario usuario;
            Console.Write("Intrduce el nombre del usuario que deseas modificar: ");
            string nombre = Console.ReadLine();
            usuario = controlador.CLeerUno(nombre);
            if (usuario != null)
            {
                Console.Write("Introducir nuevo nombre: ");
                string nuevoNombre = Console.ReadLine();
                float nuevaAltura = Comprobador.IntroducirNumero<float>("Introducir nueva altura: ","Error, el dato debe ser un número real");
                int nuevaEdad = Comprobador.IntroducirNumero<int>("Introducir nueva edad: ","Error el dato debe ser un entero");
                Usuario mod = controlador.CModificar(nombre, nuevoNombre, nuevaAltura, nuevaEdad);
                if (mod!= null)
                {
                    Comprobador.mensajeColor("Modificado correctamente", ConsoleColor.Green);
                }
                
            }
            else
            {
                Comprobador.mensajeColor("No encontrado por " + nombre, ConsoleColor.Red);
            }
        }

        public void VMostrarTodos()
        {
            IEnumerable<Usuario> todos = controlador.CLeerTodos();
            foreach (Usuario usuario in todos)
            {
                Comprobador.mensajeColor("Usuario: " + usuario.ToString(), ConsoleColor.Green);
            }
        }

        public void VMostrarUno()
        {
              
            Usuario usuario;
            Console.WriteLine("Intrduce el nombre del usuario que deseas buscar: ");
            string nombre = Console.ReadLine();
            usuario = controlador.CLeerUno(nombre);

            if (usuario != null)
            {
                Comprobador.mensajeColor("Usuario: " + usuario.ToString(), ConsoleColor.Green);
            }
            else
            {
                Comprobador.mensajeColor("No encontrado por " + nombre, ConsoleColor.Red);
            }
        }

        private int PedirOpcion(string mensajeError)
        {
            bool errorFlag;
            string linea;
            int varNum;
            do
            {

                Console.WriteLine("Opciones: ");
                Console.WriteLine();
                Console.WriteLine("    (1) Añadir nuevo usuario.");
                Console.WriteLine();
                Console.WriteLine("    (2) Mostrar un usuario.");
                Console.WriteLine();
                Console.WriteLine("    (3) Mostrar todos los usuarios.");
                Console.WriteLine();
                Console.WriteLine("    (4) Modificar un usuario.");
                Console.WriteLine();
                Console.WriteLine("    (5) Eliminar un usuario.");
                Console.WriteLine();
                Console.Write("Elige una opción: ");

                linea = Console.ReadLine();

                errorFlag = !int.TryParse(linea, out varNum);
                if (varNum < 1 && varNum > 5)
                {
                    errorFlag = true;
                }

                if (errorFlag)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(mensajeError);
                    Console.ForegroundColor = ConsoleColor.White;
                }

            } while (errorFlag);
            return varNum;
        }

        private void GestionarOpcion(int opcion)
        {
            switch (opcion)
            {
                case 1:
                    VCrear();
                    break;
                case 2:
                    VMostrarUno();
                    break;
                case 3:
                    VMostrarTodos();
                    break;
                case 4:
                    VModificar();
                    break;
                case 5:
                    VEliminar();
                    break;
                default:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("OPCIÓN NO VÁLIDA");
                    Console.ForegroundColor = ConsoleColor.White;
                    throw new ArgumentOutOfRangeException("se ha intentado acceder a una opción del menú inválida");
            }
        }
    }
}
