﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestorUsuariosMVC
{
    class Controlador
    {
        IModeloGenerico<Usuario> modelo;

        public Controlador(IModeloGenerico<Usuario> modelo)
        {
            this.modelo = modelo;
        }

        public Usuario CCrear(string nombre, float altura, int edad)
        {
            return modelo.Crear(new Usuario(nombre, altura, edad));
        }

        public IEnumerable<Usuario> CLeerTodos()
        {
            return modelo.LeerTodos();
        }

        public Usuario CLeerUno<TipoBusq>(TipoBusq campoBusq)
        {
            return modelo.LeerUno(campoBusq);
        }

        public bool CEliminar<TipoBusq>(TipoBusq campoBusq)
        {
            return modelo.Eliminar(campoBusq);
        }

        public Usuario CModificar<TipoBusq>(TipoBusq campoBusq, string nombre, float altura, int edad)
        {
            return modelo.Modificar(campoBusq, new Usuario(nombre, altura, edad));
        }
    }
}
