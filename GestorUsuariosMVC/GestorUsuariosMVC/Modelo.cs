﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.IO;
using Newtonsoft.Json;

namespace GestorUsuariosMVC
{
    public delegate List<Usuario> LoadUsers();
    public delegate void OnReadAll();
    class Modelo : IModeloGenerico<Usuario>
    {
        public Action<Usuario> OnCreate;
        public Action<string,Usuario> OnChange;
        public Action<string> OnDelete;
        public Action<string> OnRead;
        public LoadUsers loadUsers;
        public OnReadAll onReadAll;
        List<Usuario> listaUsuario;

        static Modelo instancia;

        bool estanCargados = false;
        public static Modelo Instancia
        {
            get
            {
                if (instancia == null)
                {
                    instancia = new Modelo();
                }
                return instancia;
            }
        }

        private void cargarUsuarios()
        {
            if (!estanCargados)
            {
                listaUsuario = loadUsers?.Invoke();
                estanCargados = true;
            }
        }

        private Modelo()
        {
            listaUsuario = new List<Usuario>();
        }

        public Usuario Crear(Usuario nuevoObj)
        {
            cargarUsuarios();
            OnCreate?.Invoke(nuevoObj);
            listaUsuario.Add(nuevoObj);
            return listaUsuario[listaUsuario.Count-1];
        }

        public bool Eliminar<TipoBusq>(TipoBusq campoBusq)
        {
            cargarUsuarios();
            if (typeof(TipoBusq) == typeof(string))
            {
                OnDelete?.Invoke((string)(object)campoBusq);
                return listaUsuario.Remove(LeerUno(campoBusq));
            }
            return false;
        }

        public IList<Usuario> LeerTodos()
        {
            cargarUsuarios();
            onReadAll?.Invoke();
            return listaUsuario;

        }

        public Usuario LeerUno<TipoBusq>(TipoBusq campoBusq)
        {
            cargarUsuarios();
            if (typeof(TipoBusq) == typeof(string))
            {
                OnRead?.Invoke((string)(object)campoBusq);
                foreach (var item in listaUsuario)
                {
                    if (item.Nombre == (string)(object)campoBusq)
                    {
                        return item;
                    }
                }
            }
            return null;
        }

        public Usuario Modificar<TipoBusq>(TipoBusq campoBusq, Usuario nuevoObjeto)
        {
            cargarUsuarios();
            if (typeof(TipoBusq) == typeof(string))
            {
                OnChange?.Invoke((string)(object)campoBusq,nuevoObjeto);
                Usuario user = LeerUno(campoBusq);
                if (user == null)
                {
                    return null;
                }
                else
                {
                    int pos = listaUsuario.IndexOf(user);
                    listaUsuario[pos] = nuevoObjeto;
                    return listaUsuario[pos];
                }
            }
            return null;
        }
    }
}
