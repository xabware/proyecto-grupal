﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GestorUsuariosMVC
{
    class Persistencia
    {
        static string path = "..\\..\\..\\losUsuarios.txt";
        static string separador = "##########LINEA SEPARADORA DE USUARIOS##########\n";
        public Persistencia()
        {
            Modelo modelo = Modelo.Instancia;
            modelo.OnCreate += (Usuario user) =>
            {
                string yeison = JsonConvert.SerializeObject(user, Formatting.Indented);
                File.AppendAllText(path, yeison + separador);
            };

            modelo.loadUsers += () =>
            {
                List<Usuario> lista = new List<Usuario>();
                lista = cargarDeFichero();
                return lista;
            };

            modelo.OnDelete += (string str) =>
            {
                List<Usuario> lista = new List<Usuario>();
                lista = cargarDeFichero();
                string nuevoFichero = "";
                Usuario usuario = lista.Find(u => u.Nombre == str);
                lista.Remove(usuario);
                nuevoFichero = serializar(lista);
                File.Delete(path);
                File.AppendAllText(path, nuevoFichero);
            };

            modelo.OnChange += (string str,Usuario nuevo) =>
            {
                List<Usuario> lista = new List<Usuario>();
                lista = cargarDeFichero();
                string nuevoFichero = "";
                Usuario usuario = lista.Find(u => u.Nombre == str);
                lista[lista.IndexOf(usuario)] = nuevo;
                nuevoFichero = serializar(lista);
                File.Delete(path);
                File.AppendAllText(path, nuevoFichero);
            };
        }

        

        private string serializar(List<Usuario> lista)
        {
            string res="";
            foreach (Usuario user in lista)
            {
                res += JsonConvert.SerializeObject(user, Formatting.Indented)+separador;
            }
            return res;
        }

        private List<Usuario> cargarDeFichero()
        {
            List<Usuario> lista= new List<Usuario>();
            if (File.Exists(path))
            {
                string text = File.ReadAllText(path);
                string[] arrayUsuarios = text.Split(separador);
                foreach (string strUsuario in arrayUsuarios)
                {
                    Usuario user = JsonConvert.DeserializeObject<Usuario>(strUsuario);
                    if (user != null)
                    {
                        lista.Add(user);
                    }
                }
                return lista;
            }
            else
            {
                return lista;
            }
            
        }
    }
}
