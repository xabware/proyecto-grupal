﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica01_GestionDeUsuario
{
    public class UIConsole
    {

        public static void PedirString(string nombreDato, out string varDato)
        {
            Console.WriteLine("Introduce el campo " + nombreDato + ": ");
            varDato = Console.ReadLine();
            
        }

        public static void PedirNum<Tipo>(string nombreDato, out Tipo varDato)
        {
            Console.WriteLine("Introduce el campo " + nombreDato + ": ");
            string str = Console.ReadLine();
            bool isValueOK;

            if (typeof(Tipo) == typeof(int))
            {
                int entero;
                isValueOK = int.TryParse(str, out entero);
                varDato = (Tipo)(object)entero;
            }
            else if (typeof(Tipo) == typeof(float))
            {
                float flotante;
                isValueOK = float.TryParse(str, out flotante);
                varDato = (Tipo)(object)flotante;
            }
            else if (typeof(Tipo) == typeof(double))
            {
                double doble;
                isValueOK = double.TryParse(str, out doble);
                varDato = (Tipo)(object)doble;
            }
            else
            {
                varDato = (Tipo)(object)0;
                isValueOK = true;
                Console.Error.WriteLine("ERROR: tipo de dato no valido");
            }

            while (!isValueOK)
            {
                Console.WriteLine("por favor, Introduzca un numero.");
                str = Console.ReadLine();
                if (typeof(Tipo) == typeof(int))
                {
                    int entero;
                    isValueOK = int.TryParse(str, out entero);
                    varDato = (Tipo)(object)entero;
                }
                else if (typeof(Tipo) == typeof(float))
                {
                    float flotante;
                    isValueOK = float.TryParse(str, out flotante);
                    varDato = (Tipo)(object)flotante;
                }
                else if (typeof(Tipo) == typeof(double))
                {
                    double doble;
                    isValueOK = double.TryParse(str, out doble);
                    varDato = (Tipo)(object)doble;
                }

            }
        }


        public static Tipo IntroducirNumero<Tipo>(string mensajeError)
        {
            bool errorFlag;
            string linea;
            Tipo varNum;
            do
            {

                Console.WriteLine("Opciones: ");
                Console.WriteLine();
                Console.WriteLine("    (1) Añadir nuevo usuario.");
                Console.WriteLine();
                Console.WriteLine("    (2) Mostrar un usuario.");
                Console.WriteLine();
                Console.WriteLine("    (3) Mostrar todos los usuarios.");
                Console.WriteLine();
                Console.WriteLine("    (4) Modificar un usuario.");
                Console.WriteLine();
                Console.WriteLine("    (5) Eliminar un usuario.");
                Console.WriteLine();
                Console.Write("Elige una opción: ");

                linea = Console.ReadLine();

                if (typeof(Tipo) == typeof(int))
                {
                    errorFlag = !int.TryParse(linea, out int tempInt);
                    if (tempInt < 1 && tempInt > 5)
                    {
                        errorFlag = true;
                    }
                    varNum = (Tipo)(object)tempInt;

                }
                else if (typeof(Tipo) == typeof(float))
                {
                    errorFlag = !float.TryParse(linea, out float tempFloat);
                    varNum = (Tipo)(object)tempFloat;
                }
                else if (typeof(Tipo) == typeof(double))
                {
                    errorFlag = !double.TryParse(linea, out double tempDouble);
                    varNum = (Tipo)(object)tempDouble;
                }
                else
                {
                    Console.Error.WriteLine("ERROR: Tipo de dato no valido");
                    throw new FormatException("ERROR: Tipo de dato no valido");
                }




                if (errorFlag)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(mensajeError);
                    Console.ForegroundColor = ConsoleColor.White;
                }

            } while (errorFlag);
            return varNum;
        }

        public static void GestionarOpcion(int opcion)
        {
            GestionUsuario gestor = new GestionUsuario();
            string entrada;
            switch (opcion)
            {
                case 1:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Añadir nuevo usuario.");
                    Console.ForegroundColor = ConsoleColor.White;
                    gestor.CrearUno();
                    break;
                case 2:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Mostrar un usuario.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("Introduce el nombre de un usuario: ");
                    entrada = Console.ReadLine();
                    gestor.MostrarUno(entrada);
                    break;
                case 3:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Mostrar todos los usuarios.");
                    Console.ForegroundColor = ConsoleColor.White;
                    gestor.MostrarTodos();
                    break;
                case 4:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Modificar un usuario.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("Introduce el nombre de un usuario: ");
                    entrada = Console.ReadLine();
                    gestor.ModificarUno(entrada);
                    break;
                case 5:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Eliminar un usuario.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("Introduce el nombre de un usuario: ");
                    entrada = Console.ReadLine();
                    gestor.EliminarUno(entrada);
                    break;
                default:
                    Console.WriteLine("¿Cómo has llegado aquí?");
                    break;
            }
        }

        /*
         * 
         * Ya no hacen falta
         * 
        public static void PedirFloat(string nombreDato, out float varDato)
        {
            Console.WriteLine("Introduce el campo " + nombreDato + ": ");
            string str = Console.ReadLine();
            bool isValueOK = float.TryParse(str, out varDato);

            while (!isValueOK)
            {
                Console.WriteLine("por favor, Introduzca un numero.");
                str = Console.ReadLine();
                isValueOK = float.TryParse(str, out varDato);

            }
        }

        public static void PedirInt(string nombreDato, out int varDato)
        {
            Console.WriteLine("Introduce el campo " + nombreDato + ": ");
            string str = Console.ReadLine();
            bool isValueOK = int.TryParse(str, out varDato);

            while (!isValueOK)
            {
                Console.WriteLine("por favor, Introduzca un numero.");
                str = Console.ReadLine();
                isValueOK = int.TryParse(str, out varDato);

            }
        }*/

    }
}
