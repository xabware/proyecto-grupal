﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica01_GestionDeUsuario
{
    public class GestionUsuario
    {

        List<Usuario> listaUsuario;

        public GestionUsuario()
        {
            this.listaUsuario = new List<Usuario>();
        }

        /// <summary>
        /// Al introducir un entero muestra el usuario en esa posición de la lista, 
        /// al introducir un string muestra el primer usuario en la lista con ese nombre, 
        /// al introducir un usuario, muestra ese usuario de la lista
        /// </summary>
        public void MostrarUno<Tipo>(Tipo dato)
        {
            Usuario resultado = getUsuarioFromType(dato);
            if(resultado != null)
            {
                resultado.MostrarDatosConsola();
            }
        }

        /// <summary>
        /// Muestra todos los usuarios de la lista
        /// </summary>
        public void MostrarTodos()
        {
            foreach (Usuario user in listaUsuario)
            {
                user.MostrarDatosConsola();
            }
        }

        /// <summary>
        /// Al introducir un entero elimina el usuario en esa posición de la lista, 
        /// al introducir un string elimina el primer usuario en la lista con ese nombre, 
        /// al introducir un usuario, elimina ese usuario de la lista
        /// </summary>
        public void EliminarUno<Tipo>(Tipo dato)
        {
            Usuario resultado = getUsuarioFromType(dato);
            if (resultado != null)
            {
                listaUsuario.Remove(resultado);
            }
        }

        /// <summary>
        /// Crea un nuevo usuario por consola
        /// </summary>
        public void CrearUno()
        {
            Usuario usuario = new Usuario();
            usuario.PedirDatosConsola();
            listaUsuario.Add(usuario);
        }

        /// <summary>
        /// Al introducir un entero modifica el usuario en esa posición de la lista, 
        /// al introducir un string modifica el primer usuario en la lista con ese nombre, 
        /// al introducir un usuario, modifica ese usuario de la lista
        /// </summary>
        public void ModificarUno<Tipo>(Tipo dato)
        {
            Usuario resultado = getUsuarioFromType(dato);
            if (resultado != null)
            {
                resultado.PedirDatosConsola();
            }
        }

        private Usuario getUsuarioFromType<Tipo>(Tipo dato)
        {
            Usuario resultado = null;
            if (typeof(Tipo) == typeof(int))
            {
                resultado = listaUsuario[(int)(object)dato];
            }
            else if (typeof(Tipo) == typeof(string))
            {
                foreach (Usuario user in listaUsuario)
                {
                    if (user.Nombre == (string)(object)dato)
                    {
                        resultado = user;
                        break;
                    }
                }
                if (resultado == null)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Error.WriteLine("ERROR: No hay usuarios con este nombre");
                    Console.ForegroundColor = ConsoleColor.White;
                    resultado = null;
                }
            }
            else if (typeof(Tipo) == typeof(Usuario))
            {
                resultado = (Usuario)(object)dato;
            }
            else
            {
                Console.Error.WriteLine("ERROR: Tipo de dato no valido");
                throw new FormatException("ERROR: Tipo de dato no valido");
            }
            return resultado;
        }
    }
}
