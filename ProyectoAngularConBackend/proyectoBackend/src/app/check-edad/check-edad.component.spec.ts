import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckEdadComponent } from './check-edad.component';

describe('CheckEdadComponent', () => {
  let component: CheckEdadComponent;
  let fixture: ComponentFixture<CheckEdadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckEdadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckEdadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
