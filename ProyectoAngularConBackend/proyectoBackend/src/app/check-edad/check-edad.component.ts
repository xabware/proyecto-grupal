import { Component, OnInit } from '@angular/core';
import { ServicioUsuariosService } from '../servicio-usuarios.service';

@Component({
  selector: 'app-check-edad',
  templateUrl: './check-edad.component.html',
  styleUrls: ['./check-edad.component.css']
})
export class CheckEdadComponent implements OnInit {

  constructor(public srvUser: ServicioUsuariosService) { }

  ngOnInit(): void {
  }

}
