import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckActivoComponent } from './check-activo.component';

describe('CheckActivoComponent', () => {
  let component: CheckActivoComponent;
  let fixture: ComponentFixture<CheckActivoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckActivoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckActivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
