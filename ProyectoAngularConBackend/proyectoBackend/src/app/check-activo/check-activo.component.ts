import { Component, OnInit } from '@angular/core';
import { ServicioUsuariosService } from '../servicio-usuarios.service';

@Component({
  selector: 'app-check-activo',
  templateUrl: './check-activo.component.html',
  styleUrls: ['./check-activo.component.css']
})
export class CheckActivoComponent implements OnInit {

  constructor(public srvUser: ServicioUsuariosService) { }

  ngOnInit(): void {
  }

}
