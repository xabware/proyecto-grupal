import { Injectable } from '@angular/core';
import { Usuario } from './usuario';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServicioUsuariosService {

  private arrayUsers: Array<Usuario> = [];
  private url = "/api/Usuarios"

  constructor(private clienteHTTP: HttpClient) {
    let observ: Observable<Array<Usuario>>;
    observ = this.clienteHTTP.get<Array<Usuario>>(this.url);
    observ.subscribe((datos: Array<Usuario>)=>{
      this.arrayUsers=datos;
    })
   }

  public usuariosArray(){
    return this.arrayUsers;
  }

  numMayores(): number{
    let i:number=0;
    for (let u of this.arrayUsers){
      if(u.edad>=18){
        i++;
      }
    }
    return i;
  }
  
  numMenores(): number{
    let i:number=0;
    for (let u of this.arrayUsers){
      if(u.edad<18){
        i++;
      }
    }
    return i;
  }
  
  numActivos(): number{
    let i:number=0;
    for (let u of this.arrayUsers){
      if(u.activo){
        i++;
      }
    }
    return i;
  }

  numInactivos(): number{
    let i:number=0;
    for (let u of this.arrayUsers){
      if(!u.activo){
        i++;
      }
    }
    return i;
  }
}
