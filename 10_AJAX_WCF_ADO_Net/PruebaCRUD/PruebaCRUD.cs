using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace PruebaCRUD
{
    public class PruebaCRUD
    {
        private static List<string> listaNombresBuenos = new List<string>() { "Aitor Perez", "Armando Garcia", "Elsa Martin", "Elvis Presley", "Elton John", "Elsa Frozen", "Elvis Tek"};
        static string rutaSpa;
        FirefoxDriver driver;
        IWebElement nombre, edad, altura, activo, btnAnadir, btnModificar, btnCancelar, crud, contadores;
        Process _process;
        private void StartServer()
        {
            _process = new Process
            {
                StartInfo =
                {
                    FileName =@"dotnet.exe",
                    Arguments = "run --project C:\\Users\\pmpcurso1\\Desktop\\proyecto-grupal.git\\10_AJAX_WCF_ADO_Net\\Usuarios_CRUD\\UsuariosWeb_APIRest\\UsuariosWeb_APIRest.csproj"
                }
            };
            _process.Start();
        }

        [OneTimeSetUp]
        public void InicializarClaseTest()
        {
            StartServer();
            Thread.Sleep(5000);
            rutaSpa = "http://localhost:5000/index.html";
            string fichFirefox = "..\\..\\..\\..\\FirefoxPortable\\App\\Firefox64\\firefox.exe";
            if (!File.Exists(fichFirefox))
            {
                string instalador = "..\\..\\..\\..\\FirefoxPortable_92.0_English.paf.exe";
                if (File.Exists(instalador))
                {
                    System.Diagnostics.Process.Start(instalador);
                }
            }
            if (File.Exists(fichFirefox))
            {
                FirefoxDriverService geckoService = FirefoxDriverService.CreateDefaultService("./");
                geckoService.Host = "::1";
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.BrowserExecutableLocation = fichFirefox;
                firefoxOptions.AcceptInsecureCertificates = true;

                driver = new FirefoxDriver(geckoService, firefoxOptions);
            }
        }

        [OneTimeTearDown]
        public void FinalizarClaseTest()
        {
            driver.Close();
            _process.Kill();
        }


        [SetUp]
        public void setup()
        {
            driver.Navigate().GoToUrl(rutaSpa);
            cargarElementos();
            
        }

        void cargarElementos()
        {
            nombre = driver.FindElement(By.Id("nombre"));
            edad = driver.FindElement(By.Id("edad"));
            altura = driver.FindElement(By.Id("altura"));
            activo = driver.FindElement(By.Id("activo"));
            btnAnadir = driver.FindElement(By.Id("btn-anadir"));
            btnModificar = driver.FindElement(By.Id("btn-modificar"));
            btnCancelar = driver.FindElement(By.Id("btn-cancelar"));
        }

        //Teses
        [Test]
        public void CrearUsuarios()
        {
            List<int> listaIds = new List<int>();
            int tOriginal = TableSize();
            List<string> listaNombres = listaNombresBuenos.GetRandomElements<string>(5);
            listaIds.Add(Anadir_o_modificar(listaNombres[0], 24, 240, true, btnAnadir));
            AssertExisteUsuario(listaNombres[0], 24, 240, true);

            listaIds.Add(Anadir_o_modificar(listaNombres[1], 99, 200, false, btnAnadir));
            AssertExisteUsuario(listaNombres[1], 99, 200, false);

            Anadir_o_modificar(listaNombres[2], 999, 999, false, btnAnadir);
            AssertNoExisteUsuario(listaNombres[2], 999, 999, false);

            listaIds.Add(Anadir_o_modificar(listaNombres[3], 99, 99, false, btnAnadir));
            AssertExisteUsuario(listaNombres[3], 99, 99, false);

            listaIds.Add(Anadir_o_modificar(listaNombres[4], 10, 20, true, btnAnadir));
            AssertExisteUsuario(listaNombres[4], 10, 20, true);

            listaIds.Add(Anadir_o_modificar(listaNombres[2], 55, 30, false, btnAnadir));
            AssertExisteUsuario(listaNombres[2], 55, 30, false);

            Assert.AreEqual(TableSize(), tOriginal + 5, "el tama�o de la tabla es incorrecto tras a�adir usuarios");

            driver.Navigate().Refresh();
            //cargarElementos();
            Thread.Sleep(300);
            AssertExisteUsuario(listaNombres[0], 24, 240, true);
            AssertExisteUsuario(listaNombres[1], 99, 200, false);
            AssertNoExisteUsuario(listaNombres[2], 999, 999, false);
            AssertExisteUsuario(listaNombres[3], 99, 99, false);
            AssertExisteUsuario(listaNombres[4], 10, 20, true);
            AssertExisteUsuario(listaNombres[2], 55, 30, false);
            Assert.AreEqual(TableSize(), tOriginal + 5, "el tama�o de la tabla es incorrecto tras refrecar el navegador");
            for (int i =0; i < 5; i++)
            {
                eliminarUsuario(listaIds[i],true);
            }
            Assert.AreEqual(TableSize(), tOriginal, "No se ha podido eliminar a los usuarios");
        }

        [Test]
        public void Editar()
        {
            List<string> listaNombres = listaNombresBuenos.GetRandomElements<string>(1);
            int tOriginal = TableSize();

            int id = Anadir_o_modificar(listaNombres[0], 21, 180, true, btnAnadir);
            AssertExisteUsuario(listaNombres[0], 21, 180, true);
            Assert.AreEqual(TableSize(), tOriginal + 1, "el tama�o de la tabla es incorrecto");

            editarUsuario(id, listaNombres[0] + "mod", 22, 177, true, true);
            AssertExisteUsuario(listaNombres[0] + "mod", 22, 177, true);
            Assert.AreEqual(TableSize(), tOriginal + 1, "el tama�o de la tabla es incorrecto");

            editarUsuario(id, listaNombres[0], 22, 130, true, true);
            AssertExisteUsuario(listaNombres[0], 22, 130, true);
            Assert.AreEqual(TableSize(), tOriginal + 1, "el tama�o de la tabla es incorrecto");

            editarUsuario(id, listaNombres[0] + "mod", 80, 80, true, true);
            AssertExisteUsuario(listaNombres[0] + "mod", 80, 80, true);
            Assert.AreEqual(TableSize(), tOriginal + 1, "el tama�o de la tabla es incorrecto");

            editarUsuario(id, listaNombres[0] + "mod", 22, 177, true, false);
            Assert.AreEqual(TableSize(), tOriginal + 1, "el tama�o de la tabla es incorrecto");
            eliminarUsuario(id, true);
        }

        [Test]
        public void Eliminar()
        {
            int tOriginal = TableSize();
            List<string> listaNombres = listaNombresBuenos.GetRandomElements<string>(5);
            List<int> listaIds = new List<int>();
            listaIds.Add(Anadir_o_modificar(listaNombres[0], 21, 180, true, btnAnadir));
            AssertExisteUsuario(listaNombres[0], 21, 180, true);
            Assert.AreEqual(TableSize(), tOriginal + 1, "el tama�o de la tabla es incorrecto");

            listaIds.Add(Anadir_o_modificar(listaNombres[1], 21, 180, true, btnAnadir));
            AssertExisteUsuario(listaNombres[1], 21, 180, true);
            Assert.AreEqual(TableSize(), tOriginal + 2, "el tama�o de la tabla es incorrecto");

            listaIds.Add(Anadir_o_modificar(listaNombres[2], 21, 180, true, btnAnadir));
            AssertExisteUsuario(listaNombres[2], 21, 180, true);
            Assert.AreEqual(TableSize(), tOriginal + 3, "el tama�o de la tabla es incorrecto");

            listaIds.Add(Anadir_o_modificar(listaNombres[3], 21, 180, true, btnAnadir));
            AssertExisteUsuario(listaNombres[3], 21, 180, true);
            Assert.AreEqual(TableSize(), tOriginal + 4, "el tama�o de la tabla es incorrecto");

            eliminarUsuario(listaIds[0], false);
            Assert.AreEqual(TableSize(), tOriginal + 4, "el tama�o de la tabla es incorrecto");

            eliminarUsuario(listaIds[0], true);
            Assert.AreEqual(TableSize(), tOriginal + 3, "el tama�o de la tabla es incorrecto");

            eliminarUsuario(listaIds[1], true);
            Assert.AreEqual(TableSize(), tOriginal + 2, "el tama�o de la tabla es incorrecto");

            eliminarUsuario(listaIds[2], true);
            Assert.AreEqual(TableSize(), tOriginal + 1, "el tama�o de la tabla es incorrecto");

            eliminarUsuario(listaIds[3], true);
            Assert.AreEqual(TableSize(), tOriginal, "el tama�o de la tabla es incorrecto");
        }


        [Test]
        public void A�adirContadorActivo()
        {
            var x = driver.FindElements(By.XPath("/html/body/app-root/app-check-activo/p[2]"));
            string num = x[x.Count-1].Text;
            var numActivosAnt = Convert.ToInt32(num);
            crud.Click();

            List<string> listaNombres = listaNombresBuenos.GetRandomElements<string>(5);
            Anadir_o_modificar(listaNombres[3], 24, 240, true, btnAnadir);
            contadores.Click();

            var y = driver.FindElements(By.XPath("/html/body/app-root/app-check-activo/p[2]"));
            string num2 = y[y.Count-1].Text;
            var numActivosDesp = Convert.ToInt32(num2);
            crud.Click();

            Assert.AreEqual(numActivosDesp, numActivosAnt + 1, "el tama�o del contador de activos es incorrecto");

        }

        [Test]
        public void A�adirContadorMenores()
        {
            var x = driver.FindElements(By.XPath("/html/body/app-root/app-check-edad/p[3]"));
            string num = x[x.Count-1].Text;
            var numMenoresAnt = Convert.ToInt32(num);
            crud.Click();
            List<string> listaNombres = listaNombresBuenos.GetRandomElements<string>(5);
            Anadir_o_modificar(listaNombres[3], 24, 240, true, btnAnadir);
            contadores.Click();
            var y = driver.FindElements(By.XPath("/html/body/app-root/app-check-edad/p[3]"));
            string num2 = y[y.Count-1].Text;
            var numMenoresDesp = Convert.ToInt32(num2);
            crud.Click();
            Assert.AreEqual(numMenoresDesp, numMenoresAnt  + 1, "el tama�o del contador de menores es incorrecto");

        }
        
        [Test]
        public void EditarContadorMenores()
        {
            var x = driver.FindElements(By.XPath("/html/body/app-root/app-check-edad/p[3]"));
            string numM = x[x.Count-1].Text;
            var numMenoresAnt = Convert.ToInt32(numM);   
            
            var x2 = driver.FindElements(By.XPath("/html/body/app-root/app-check-edad/p[3]"));
            string numA = x2[x2.Count-1].Text;
            var numAdultosAnt = Convert.ToInt32(numA);
            crud.Click();

            List<string> listaNombres = listaNombresBuenos.GetRandomElements<string>(5);
            Anadir_o_modificar(listaNombres[3], 24, 240, true, btnAnadir);
            
            contadores.Click();
            var y = driver.FindElements(By.XPath("/html/body/app-root/app-check-edad/p[3]"));
            string num2 = y[y.Count-1].Text;
            var numMenoresDesp = Convert.ToInt32(num2);
            crud.Click();
            Assert.AreEqual(numMenoresDesp, numMenoresAnt  + 1, "el tama�o del contador de menores es incorrecto");

        }


        //Funciones
        public void editarUsuario(int idUsuario, string NombreNuevo, int intEdad, int intAltura, bool boolActivo, bool boolModif)
        {
            var idsTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[1]"));
            var botonesEditar = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[7]"));

            int i = 0;
            foreach (var id in idsTabla)
            {
                if (id.Text == idUsuario.ToString())
                {
                    botonesEditar[i].Click();
                    //cargarElementos();
                    break;
                }
                i++;
            }
            if (i > idsTabla.Count)
            {
                Assert.Fail("No se ha encontrado el usuario");
            }

            if (boolModif)
            {
                Anadir_o_modificar(NombreNuevo, intEdad, intAltura, boolActivo, btnModificar);
            } else
            {
                btnCancelar.Click();
            }

        }

        public void eliminarUsuario(int idUsuarioEliminar, bool eliminar)
        {
            var idsTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[1]"));
            var botonesEliminar = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[6]"));

            int i = 0;
            foreach (var id in idsTabla)
            {
                if (id.Text == idUsuarioEliminar.ToString())
                {
                    botonesEliminar[i].Click();
                    break;
                }
                i++;
            }

            if (eliminar) {
                driver.SwitchTo().Alert().Accept();
                Thread.Sleep(200);
            }
            else
            {
                driver.SwitchTo().Alert().Dismiss();
            }
        }

        private int GetLastId()
        {
            Thread.Sleep(200);
            var idsTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[1]"));
            string ultimoIdString = idsTabla[idsTabla.Count-1].Text;
            return int.Parse(ultimoIdString);
        }

        private int Anadir_o_modificar(string strNombre, int intEdad, int intAltura, bool boolActivo, IWebElement btn)
        {
            limpiarCampos();
            nombre.SendKeys(strNombre);
            edad.SendKeys(Keys.Right + Keys.Backspace + intEdad);
            altura.SendKeys(Keys.Right + Keys.Backspace + intAltura);
            if (boolActivo)
            {
                activo.Click();
            }
            btn.Click();
            Thread.Sleep(100);
            return GetLastId();
        }

        private int TableSize()
        {
            return driver.FindElements(By.XPath("/html/body/table/tbody/tr")).Count;
        }

        private void AssertExisteUsuario(string nombreUsuario, int edadUsuario, int alturaUsuario, bool isActivoUsuario)
        {
            var nombresTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[2]"));
            var edadesTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[3]"));
            var alturasTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[4]"));
            var activosTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[5]/input"));
            int posicion = -1;
            
            for (int i = 0; i < nombresTabla.Count; i++)
            {
                Thread.Sleep(200);
                if (nombresTabla[i].Text == nombreUsuario &&
                   edadesTabla[i].Text == edadUsuario.ToString() &&
                   alturasTabla[i].Text == alturaUsuario.ToString() &&
                   bool.Parse(activosTabla[i].GetProperty("checked")) == isActivoUsuario)
                {
                    posicion = i;
                    return;
                }
            }
            Assert.Fail("El usuario no est� en la tabla");
        }

        private void AssertNoExisteUsuario(string nombreUsuario, int edadUsuario, int alturaUsuario, bool isActivoUsuario)
        {
            var nombresTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[2]"));
            var edadesTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[3]"));
            var alturasTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[4]"));
            var activosTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[5]/input"));
            Thread.Sleep(200);
            for (int i = 0; i < nombresTabla.Count; i++)
            {
                if (nombresTabla[i].Text == nombreUsuario &&
                   edadesTabla[i].Text == edadUsuario.ToString() &&
                   alturasTabla[i].Text == alturaUsuario.ToString() &&
                   bool.Parse(activosTabla[i].GetProperty("checked")) == isActivoUsuario)
                {
                    Assert.Fail("El usuario est� en la tabla");
                }
            }
        }

        private void limpiarCampos()
        {
            nombre.Clear();
            edad.Clear();
            altura.Clear();
            if (activo.Selected)
            {
                activo.Click();
            }
        }        
    }

    public static class extension
    {
        public static List<T> GetRandomElements<T>(this IEnumerable<T> list, int elementsCount)
        {
            return list.OrderBy(arg => Guid.NewGuid()).Take(elementsCount).ToList();
        }
    }
    
}
