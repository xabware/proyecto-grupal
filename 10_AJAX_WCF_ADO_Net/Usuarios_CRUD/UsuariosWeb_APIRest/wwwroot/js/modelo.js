class Modelo {

    constructor() {
        this.actualizar();
        this.alActualizar = () => {}
    }

    actualizar() {
        this.usuarios = [];
        this.cargarUsuarios();
    }

    borrarUsuario(id,nombre) {
        var confirmacion = confirm("¿Seguro que quieres eliminar el usuario " + nombre + "?");
        if (!confirmacion) {
            return;
        }

        let opcionesPOST = {
            method: "DELETE",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json"
                // "Content-Type": "application/x-www-form-urlencoded"
            },
        };
        // Esta otra forma de usar AJAX, devuelve una promesa.
        let promesaAJAX = fetch("/api/Usuarios/eliminar/"+id, opcionesPOST);
        // En el futuro, unos segundos después, traerá datos.
        // Para entonces, queremos parsear el JSON mediante una función transformadora
        // y usar el objeto JS como en el anterior ejemplo

        promesaAJAX
            .then((respuesta) => respuesta.json())
            .then(resultado => { // Callback de la llamada asincrona
                if(resultado){
                    for (let i in this.usuarios) {
                        if (this.usuarios[i]["id"] === id) { 
                            this.usuarios.splice(i, 1);
                            break;
                        }
                    }
                    this.alActualizar(this.usuarios, []);
                }else{
                    console.error("No se ha conseguido eliminar al usuario: "+nombre+". Con id: "+id);
                }
                
            })
            .catch(error => {
                console.error("ERROR en petición: " + error);
            });
    }

    borrarTodo() {
        var confirmacion = confirm("¿Seguro que quieres eliminar todo?");
        if (!confirmacion) {
            return;
        }

        this.usuarios.splice(0, this.usuarios.length);
        this.guardarUsuarios();
    }

    cancelar() {
        this.editar = [];
    }

    modificarUsuario(nuevoNombre, edad, altura, activo) {

        let opcionesPOST = {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json"
                // "Content-Type": "application/x-www-form-urlencoded"
            },
            body: ` {
                "nombre": "${nuevoNombre}",
                "edad": ${edad},
                "altura":${altura},
                "activo": ${activo},
                "id": ${this.editar["id"]}
              }`
        };

        let promesaAJAX = fetch("/api/Usuarios/modificar/asdf", opcionesPOST);

        promesaAJAX
            .then((respuesta) => respuesta.text())
            .then(objWF => { // Callback de la llamada asincrona
                if (objWF === "true") {
                    console.log("usuario modificado");
                    for (let u of this.usuarios) {
                        if (u["id"] === this.editar["id"]) {
                            u["nombre"] = nuevoNombre;
                            u["edad"] = edad;
                            u["altura"] = altura;
                            u["activo"] = activo;
                            this.editar = [];
                            this.actualizar();
                            return;
                        }
                        alert("El usuario no está en la tabla así que no se puede modificar")
                    }
                } else {
                    console.error("ERROR al modificar usuario");
                }
                
            })
            .catch(error => {
                console.error("ERROR en petición: " + error);
            });


    }

    cargarModificar(id) {
        for (let u of this.usuarios) {
            if (u["id"] === id) {
                this.editar = u;
                return;
            }
        }
        alert("No encontrado usuario a modificar");
    }

    buscarNombre(nombre) {
        for (let i in this.usuarios) {
            if (this.usuarios[i]["nombre"].toUpperCase() === nombre.toUpperCase()) {
                return true;
            }
        }
        return false;
    }

    getUsuarios() {
        return this.usuarios;
    }

    crearUsuario(nombre, edad, altura, activo) {
        

        let opcionesPOST = {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json"
                // "Content-Type": "application/x-www-form-urlencoded"
            },
            body: ` {
                "nombre": "${nombre}",
                "edad": ${edad},
                "altura":${altura},
                "activo": ${activo}
              }`
        };

        let promesaAJAX = fetch("/api/Usuarios/crear/swdf", opcionesPOST);

        promesaAJAX
            .then((respuesta) => respuesta.json())
            .then(objWF => { // Callback de la llamada asincrona
                if (objWF != -1) {
                    this.usuarios.push(new Usuario(nombre, edad, altura, activo, objWF));
                    console.log("usuario insertado");
                    this.alActualizar(this.usuarios, []);
                } else {
                    console.error("ERROR al insertar usuario");
                }
            })
            .catch(error => {
                console.error("ERROR en petición: " + error);
            });




    }

    cargarUsuarios() {

        let opcionesPOST = {
            method: "GET",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json"
                // "Content-Type": "application/x-www-form-urlencoded"
            },
        };
        // Esta otra forma de usar AJAX, devuelve una promesa.
        let promesaAJAX = fetch("/api/Usuarios", opcionesPOST);
        // En el futuro, unos segundos después, traerá datos.
        // Para entonces, queremos parsear el JSON mediante una función transformadora
        // y usar el objeto JS como en el anterior ejemplo

        promesaAJAX
            .then((respuesta) => respuesta.json())
            .then(tmpUsuarios => { // Callback de la llamada asincrona
                this.usuarios = tmpUsuarios;
                this.alActualizar(this.usuarios, []);
                console.log(this.usuarios);
            })
            .catch(error => {
                console.error("ERROR en petición: " + error);
            });

        
    }
}