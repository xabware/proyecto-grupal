﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.Json;

using System.Text.Json.Serialization;

namespace ModeloUsuarios
{
    public enum Entorno
    {
        Ninguno = 0,
        Produccion = 1,
        Desarrollo = 2,
        Preproduccion = 3,
    }
    public class ModuloPersistenciaADO
    {
        string CONEX_BD;
        static Entorno entorno = Entorno.Desarrollo;
        private string pathBD = "C:\\DB\\bd_usu.mdf";


        public ModuloPersistenciaADO(Entorno entorno)
        {
            ModuloPersistenciaADO.entorno = entorno;

            if (entorno == Entorno.Produccion)
            {
                CONEX_BD = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename="+pathBD+";Integrated Security=True;Connect Timeout=30";
            }
            else
            {
                CONEX_BD = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\DB\\bd_usu_test.mdf;Integrated Security=True;Connect Timeout=30";
            }

            ModeloUsuario.Instancia.delegadoCrear = Crear;
            ModeloUsuario.Instancia.delegadoEliminar = Eliminar;
            ModeloUsuario.Instancia.leerTodo = LeerTodo;
            ModeloUsuario.Instancia.leerUno = LeerUno;
            ModeloUsuario.Instancia.leerConfirmar = LeerConfirmar;
            ModeloUsuario.Instancia.modificar = Modificar;
        }
        public int Crear(Usuario usuario)
        {
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();
                    SqlCommand comando = conexion.CreateCommand();
                    Random rnd = new Random();
                    comando.CommandText = $"insert into usuario (nombre,email,altura,edad,activo) values ('" +
                               $"{usuario.Nombre}', '{usuario.Nombre.ToLower().Replace(' ', '_')+ rnd.Next(9999999).ToString() + "@email.es"}', {usuario.Altura.ToString().Replace(',', '.')}, {usuario.Edad},{(usuario.Activo ? 1 : 0)})";
                    int filasAfectadas = comando.ExecuteNonQuery();
                    if (filasAfectadas != 1)
                    {
                        throw new Exception("No se ha añadido al usuario"
                            + comando.CommandText);
                    }
                    SqlCommand comandoId = conexion.CreateCommand();
                    comandoId.CommandText = "SELECT SCOPE_IDENTITY() AS id";
                    SqlDataReader lectorDR = comandoId.ExecuteReader();
                    lectorDR.Read();
                    int nuevoId = decimal.ToInt32((decimal)lectorDR["id"]);
                    return nuevoId;
                }   // conexion.Close();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
            }
            return -1;
        }

        public bool Eliminar(int entero)
        {
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();
                    SqlCommand borrarCompras = conexion.CreateCommand();
                    borrarCompras.CommandText = "DELETE FROM CompraUsuProd WHERE idUsuario = " + entero;
                    borrarCompras.ExecuteNonQuery();
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "DELETE FROM Usuario WHERE Id = " + entero;
                    int filasAfectadas = comando.ExecuteNonQuery();
                    if (filasAfectadas != 1)
                    {
                        return false;
                        //throw new Exception("No se ha eliminado al usuario con ID: " + entero + comando.CommandText);
                    }
                    return true;
                }   // conexion.Close();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
                return false;
            }
        }

        //TODO: Revisar estas dos funciones identicas
        public IList<Usuario> LeerTodo()
        {
            IList<Usuario> listaUsuarios = new List<Usuario>();
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();

                    Usuario usuario;
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "SELECT Id, nombre, altura, edad, activo FROM Usuario";
                    SqlDataReader lectorDR = comando.ExecuteReader();

                    while (lectorDR.Read())
                    {
                        usuario = GetUserFromReader(lectorDR);

                        listaUsuarios.Add(usuario);
                    }
                    Console.WriteLine("Leidos de bbdd:" + listaUsuarios.Count);
                }   // conexion.Close();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
            }
            return listaUsuarios;
        }

        public Usuario LeerUno(int entero)
        {
            try
            {
                SqlConnection conexion;
                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "SELECT Id, nombre, altura, edad, activo FROM Usuario WHERE Id="+entero;
                    SqlDataReader lectorDR = comando.ExecuteReader();

                    while (lectorDR.Read())
                    {
                        Usuario usuario = GetUserFromReader(lectorDR);
                        if (usuario.Id == entero)
                        {
                            return usuario;
                        }
                        else
                        {
                            Console.Error.WriteLine("Usuario incorrecto");
                            return null;
                        }
                            
                    }
                }   // conexion.Close();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
                return null;
            }
            return null;
        }

        public bool LeerConfirmar(string nombre)
        {
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "SELECT Id, nombre, altura, edad, activo FROM Usuario WHERE nombre=" + nombre;
                    SqlDataReader lectorDR = comando.ExecuteReader();
                    if (!lectorDR.HasRows)
                    {
                        return false;
                        throw new Exception("No se ha encontrado el usuario con nombre : " + nombre
                            + comando.CommandText);
                    }
                    return true;
                }   // conexion.Close();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
                return false;
            }
        }

        public bool Modificar(Usuario usu)
        {
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();
                    SqlCommand actualizacion = conexion.CreateCommand();
                    Random rnd = new Random();
                    actualizacion.CommandText = $"UPDATE usuario  SET nombre = '{usu.Nombre}', email = '{usu.Nombre.ToLower().Replace(' ', '_')+ rnd.Next(9999999).ToString() + "@email.es"}',edad = {usu.Edad},altura = {usu.Altura.ToString().Replace(',', '.')}, activo = {(usu.Activo ? 1 : 0)} WHERE id = {usu.Id}";
                    int numFilas = actualizacion.ExecuteNonQuery();
                    if(numFilas != 1)
                    {
                        return false;
                    }
                    Console.WriteLine("Modificado de bbdd de id:" + usu.Id);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
            }
            return false;
        }

        private Usuario GetUserFromReader(SqlDataReader lectorDR)
        {
            Usuario user = new Usuario();
            user.Nombre = lectorDR[1].ToString();
            user.Altura = (float)(double)lectorDR["altura"];
            user.Edad = lectorDR.GetByte(3);
            user.Id = (int)lectorDR["Id"];
            user.Activo = (bool)lectorDR["activo"];
            return user;
        }
    }
}
